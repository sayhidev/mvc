Rails.application.routes.draw do

  root 'landing/v1#index'

  namespace :landing do
    get '/' => 'v1#index'
  end

  namespace :investee do
    get '/' => 'dashboard#index'
    resources :projects
  end

  namespace :investor do
    get '/' => 'dashboard#index'
  end

  namespace :v1, defaults:  { format: :json } do
    resource  :login, only: [:create], controller: :sessions
    resources :users, only: [:create]
    resources :projects do 
      collection do 
        get 'investee'
        get 'investor'
      end
    end
    get 'test' => 'main#test'
  end

  devise_for :users, 
  :controllers => {:registrations => "registrations"}, 
  :path => '', 
  :path_names => {:sign_in => 'id', :sign_out => 'logout', :sign_up => 'registration'}   

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
