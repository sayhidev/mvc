# Granit Technology

# API requests

curl -X POST -H "Content-Type: application/json" -d '{"user": {"email": "test@example.com", "password": "12341234", "password_confirmation": "12341234", "usertype": 1}}' http://granit-tech.herokuapp.com/v1/users

curl -X POST -H "Content-Type: application/json" -d '{"email": "test@example.com", "password": "12341234"}' 
http://granit-tech.herokuapp.com/v1/users

curl -X POST -H "Content-type: application/json" -H "kpmqmyGyzF2ToPa743rr" -d '{ "project": { "tile": "curl"} }' http://granit-tech.herokuapp.com/v1/projects 