require 'test_helper'

class Landing::V1ControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get landing_v1_index_url
    assert_response :success
  end

end
