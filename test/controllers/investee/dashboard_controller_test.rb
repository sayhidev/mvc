require 'test_helper'

class Investee::DashboardControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get investee_dashboard_index_url
    assert_response :success
  end

end
