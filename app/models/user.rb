class User < ApplicationRecord
	devise :database_authenticatable, :registerable, 
	:recoverable, :rememberable, :trackable, :validatable

  after_create :set_user_role
  
	has_one :investee, dependent: :destroy
	has_one :investor, dependent: :destroy

	validates :usertype, presence: true

	def status
		if usertype === 1
		  "Заемщик"
		else
		  "Инвестор"
		end
	end


  private

	def set_user_role
		if usertype === 1
			Investee.create(user_id: id)
		else
			Investor.create(user_id: id)
		end
	end

end
