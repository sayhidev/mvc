module V1
  class ProjectSerializer < ActiveModel::Serializer

    attributes :id, :owner_id

    def owner_id
    	object.investee.user.id
    end

  end
end
