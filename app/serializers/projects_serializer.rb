module V1
  class ProjectsSerializer < ActiveModel::Serializer

    attributes :id, :owner_id

    def owner_id
    	object.investee.user.id
    end

  end
end
