class Investor::DashboardController < ApplicationController
  
  before_action :set_user_type

  def index
  end


  private
  def set_user_type
  	if current_user.usertype === 1
  		redirect_to :investee
  	end
  end

end
