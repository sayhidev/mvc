class RegistrationsController < Devise::RegistrationsController
 
  before_action :configure_permitted_parameters

  def new
    super
  end

  def create
    super
    @user.access_token = "#{Devise.friendly_token}"
    @user.save
  end

  def update
    super
    @user.save
  end

  def destroy
    super
    @user.destroy
  end

 private

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up).push(:email, :name, :usertype, :username)
  end

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation, :usertype, :username)
  end

  def account_update_params
    params.require(:user).permit(
      :email, :password, :password_confirmation, :usertype, :access_token, :username)
  end

protected

  def update_resource(resource, params)
    resource.update_without_password(params)
  end

end



