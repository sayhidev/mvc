class V1::ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  
  skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' } 


  # GET /v1/projects/investee
  # params access_token

  def investee
    auth_token = request.headers['Authorization']
    if auth_token
      @user = User.where(access_token: auth_token).last
      @projects = @user.investee.projects
      render json: @projects, each_serializer: V1::ProjectsSerializer, root: false
    else
     render json: 500
    end
  end

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
    render json: @projects, each_serializer: V1::ProjectsSerializer, root: false
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
  end

  # POST /v1/projects
  def create
    @project = Project.new(project_params)
    auth_token = request.headers['Authorization']
    @user = User.all.where(access_token: auth_token).last
    @project.investee_id = @user.investee.id
    if @project.save
      render json: @project
    else
      render json: 500
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:investee_id, :tile, :description, :image, :videolink, :site, :mission)
    end
end
