module V1
  class SessionsController < ApplicationController
    
    skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

    respond_to :json

    # POST /v1/login
    def create
      @user = User.find_for_database_authentication(email: params[:email])
      return invalid_login_attempt unless @user
      if @user.valid_password?(params[:password])
        sign_in :user, @user
        render json: @user, serializer: SessionSerializer, root: nil
      else
        invalid_login_attempt
      end
    end

    private

    def invalid_login_attempt
      warden.custom_failure!
      render json: {error: "Invalid login attempt"}, status: :unprocessable_entity
    end

  end
end
