module V1
  class MainController < ApplicationController
  	skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

    before_action :authenticate_user_from_token!

  	respond_to :json
  
    def test
    	render json: 200
    end
  
  private
    def authenticate_user_from_token!
      auth_token = request.headers['Authorization']
      if auth_token
        authenticate_with_auth_token auth_token
      else
        authentication_error
      end
    end

	  def authenticate_with_auth_token auth_token 
	
	    user = User.where(access_token: auth_token).first

	    if user && Devise.secure_compare(user.access_token, auth_token)
	      # User can access
	      sign_in user, store: false
	    else
	      authentication_error
	    end
	  end


    def authentication_error
      # User's token is either invalid or not in the right format
      render json: {error: "unauthorized"}, status: 401  # Authentication timeout
    end

  end
end

