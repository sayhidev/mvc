module V1
  class UsersController < ApplicationController
    # skip_before_action :authenticate_user_from_token!, only: [:create]

    skip_before_action :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

    respond_to :json

    # POST /v1/users
    # Creates an user
    def create
      @user = User.new(user_params)
      @user.access_token = "#{Devise.friendly_token}"
      if @user.save
        render json: @user, serializer: V1::SessionSerializer, root: nil
      else
        render json: "error" 
      end
    end

    private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :usertype, :username, :access_token)
    end
  end
end

