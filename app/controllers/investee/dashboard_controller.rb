class Investee::DashboardController < ApplicationController

  before_action :set_user_type

  def index
  end

  private
  def set_user_type
  	if current_user.usertype === 2
  		redirect_to :investor
  	end
  end
end
