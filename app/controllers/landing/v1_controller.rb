class Landing::V1Controller < ApplicationController
  def index
  	if signed_in?
  		if current_user.usertype === 2
  			redirect_to :investor
  		else
  			redirect_to :investee
  		end
  	end
  end
end
