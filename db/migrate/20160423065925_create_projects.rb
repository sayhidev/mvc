class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.references :investee, foreign_key: true
      t.string :tile
      t.text :description
      t.string :image
      t.string :videolink
      t.string :site
      t.text :mission

      t.timestamps
    end
  end
end
